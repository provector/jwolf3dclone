package provector.ie.jengine;

import provector.ie.jengine.components.Input;
import provector.ie.jengine.components.Sound;
import provector.ie.jengine.components.Window;
import provector.ie.jengine.rendering.Transform;
import provector.ie.jwolf3d.Level;

public class Game {
	
	private static Level level;
	private static boolean isRunning;
	private static int levelNum = -1;
	
	private static float fov = 70;
	
	public Game() {
	
		Sound.playClip("music0.wav");
		loadNextLevel();
		
		Transform.setProjection(fov,Window.getWidth(), Window.getHeight(), 0.01f, 1000f);
		Transform.setCamera(Game.getLevel().getPlayer().getCamera());
		isRunning = true;
	}
	
	public void input() {
		level.input();
		/*
		 * Custom FOV
		 */
		if(Input.getKey(Input.KEY_ADD)) {
			fov++;
			if(fov>120) {
				fov=120;
				System.out.println("Max fov(120) reached!");
			}
			Transform.setProjection(fov,Window.getWidth(), Window.getHeight(), 0.01f, 1000f);
		}
		if(Input.getKey(Input.KEY_MINUS)) {
			fov--;
			if(fov<10) {
				fov = 10;
				System.out.println("Min fov(10) reached!");
			}			
			Transform.setProjection(fov,Window.getWidth(), Window.getHeight(), 0.01f, 1000f);
		}
	}
	
	public static void loadNextLevel() {
		//TODO: I think this logic should be in the GAME class
		System.out.println("NEXT LEVEL!");
		levelNum++;
		level = new Level("TestLevel"+levelNum+".png","TexturePack1.png");
		Transform.setProjection(fov,Window.getWidth(), Window.getHeight(), 0.01f, 1000f);
		Transform.setCamera(Game.getLevel().getPlayer().getCamera());
		isRunning = true;
	}
	
	public void update() {
		
		if(isRunning) {			
			level.update();
		}		
	}
	
	public void render() {
	
		if(isRunning) {
			level.render();
		}			
	}
	
	public static void setIsRunning(boolean value) {
		isRunning=value;
	}
	
	public static Level getLevel() {
		//TODO: might not be the best for subclass access
		return level;
	}
	
	/*
	 * TODOS:
	 * [X] - Replace texture loading by slickUtil with custom method from github?
	 * [] - For now filtering is disabled, figure out a better way to load separate textures (GL_NEAREST,Texture.Class);
	 * [] - HUD
	 * [] - Collisions with enemies
	 * [] - Enemies check for player in sight before shooting
	 * [] - Handle dead monsters if not handled in tutorial
	 * [] - Closing doors sound
	 * 
	 * BUGS:
	 * [X] - Monster health draining too fast (shoot delay?)	 
	 * 
	 * === Later/Maybe
	 * [] - Level editor for the balls
	 * [] - Menu
	 */
	
}