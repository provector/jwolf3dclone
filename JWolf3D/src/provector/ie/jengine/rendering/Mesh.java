package provector.ie.jengine.rendering;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import provector.ie.jengine.components.Util;
import provector.ie.jengine.rendering.entities.Vector3f;
import provector.ie.jengine.rendering.entities.Vertex;

public class Mesh {
	
	private static final boolean CALC_NORMALS_BY_DEFAULT = true;

	private int vbo; //vertices buffer object
	private int ibo; //index buffer object
	private int size;
	
	public Mesh(String fileName,boolean calcNormals) {
		initMeshData();
		loadMesh(fileName,calcNormals);
	}
	
	public Mesh(Vertex[] vertices, int[] indices) {
		this(vertices,indices,CALC_NORMALS_BY_DEFAULT);
	}
	
	public Mesh(Vertex[] vertices, int[] indices,boolean calcNormals) {
		initMeshData();
		addVertices(vertices,indices,calcNormals);
	}
	
	private void initMeshData() {
		vbo = glGenBuffers();
		ibo = glGenBuffers();
		size = 0;
	}
	
	private void addVertices(Vertex[] vertices, int[] indices,boolean calcNormals) 
	{
		
		if(calcNormals) {
			calcNormals(vertices,indices);
		}
		
		size = indices.length;
		
		glBindBuffer(GL_ARRAY_BUFFER,vbo); //any buffer operations will affect vbo from now
		glBufferData(GL_ARRAY_BUFFER,Util.createFlippedBuffer(vertices),GL_STATIC_DRAW); //last param is data type
		
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,ibo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,Util.createFlippedBuffer(indices),GL_STATIC_DRAW);
		
	}
	
	public void draw() 
	{
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		
		glBindBuffer(GL_ARRAY_BUFFER,vbo);
		//Tell gl how to interpret the data
		//segment 0, 3 element which are floats, normalize?, how big is one vertex, where data for each vertex starts
		glVertexAttribPointer(0,3,GL_FLOAT,false,Vertex.SIZE * 4/*floating point numbers precision*/,0); 
		glVertexAttribPointer(1,2,GL_FLOAT,false,Vertex.SIZE * 4,12);//floating is 4 bytes 4*3 = 12 bytes
		glVertexAttribPointer(2,3,GL_FLOAT,false,Vertex.SIZE * 4,20);

		
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,ibo);
		glDrawElements(GL_TRIANGLES,size,GL_UNSIGNED_INT,0);
		
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);


	}
	
	private void calcNormals(Vertex[] vertices,int[] indices) {
		for(int i=0;i<indices.length;i+=3) {
			int i0 = indices[i];
			int i1 = indices[i+1];
			int i2 = indices[i+2];
			
			Vector3f v1 = vertices[i1].getPos().sub(vertices[i0].getPos());
			Vector3f v2 = vertices[i2].getPos().sub(vertices[i0].getPos());
			
			Vector3f normal = v1.cross(v2).normalize();
			vertices[i0].setNormal(vertices[i0].getNormal().add(normal));
			vertices[i1].setNormal(vertices[i1].getNormal().add(normal));
			vertices[i2].setNormal(vertices[i2].getNormal().add(normal));
						
		}//endFOR
		
		for(int i=0;i<vertices.length;i++) {
			vertices[i].setNormal(vertices[i].getNormal().normalize());
		}
	}
	
	private void loadMesh(String fileName,boolean calcNormals) {
		String[] splitArray = fileName.split("\\.");
		String ext = splitArray[splitArray.length-1];
		if(!ext.equals("obj")) {
			System.out.println("Error: File format not supported: "+ext);
			new Exception().printStackTrace();
			System.exit(1);
		}
		
		ArrayList<Vertex> vertices = new ArrayList<>();
		ArrayList<Integer> indices = new ArrayList<>();
		
		BufferedReader meshReader = null;
		try {
			meshReader = new BufferedReader(new FileReader("res/models/"+fileName));
			String line;
			while((line=meshReader.readLine())!=null) {
				 if(line.startsWith("v")) {
					 String[] tokens = line.split("\\s+");
					 vertices.add(new Vertex(new Vector3f(Float.parseFloat(tokens[1]),
							 							  Float.parseFloat(tokens[2]),
							 							  Float.parseFloat(tokens[3]))));
				 } else if (line.startsWith("f")) {
					 
					 if(line.contains("/")) //with Normals
					 {
						 String[] tokens = line.split("\\s+");
						 indices.add(Integer.parseInt(tokens[1].split("/")[0])-1);
						 indices.add(Integer.parseInt(tokens[2].split("/")[0])-1);
						 indices.add(Integer.parseInt(tokens[3].split("/")[0])-1);
						 
						 if(tokens.length>4) {
							 System.out.println("EXECUTING");
							 indices.add(Integer.parseInt(tokens[1].split("/")[0])-1);
							 indices.add(Integer.parseInt(tokens[3].split("/")[0])-1);
							 indices.add(Integer.parseInt(tokens[4].split("/")[0])-1);
						 }
						 
					 }else {
						 String[] tokens = line.split("\\s+");
						 indices.add(Integer.parseInt(tokens[1])-1);
						 indices.add(Integer.parseInt(tokens[2])-1);
						 indices.add(Integer.parseInt(tokens[3])-1);
					 }
					 
				 }				
			}
			meshReader.close();
//				System.out.println("VERTICES:"+vertices);
//				System.out.println("INDICES:"+indices);
//				System.out.println("Debug exit");
//				System.exit(1);
		}catch(IOException io) {
			System.out.println("Error while loading model file: "+fileName+": "+io.getMessage());
			io.printStackTrace();
		}		
		
		Vertex[] vertexData = new Vertex[vertices.size()];
		vertices.toArray(vertexData);
		
		//Direct translation, dont have to use Utils.toIntArray
		Integer[] indexData = new Integer[indices.size()];
		indices.toArray(indexData);
		
		addVertices(vertexData,Util.toIntArray(indexData),calcNormals);
		
	}
}
