package provector.ie.jengine.rendering.entities.lights;

import provector.ie.jengine.rendering.entities.Vector3f;

public class SpotLight {

	PointLight pointLight;
	Vector3f direction;
	private float cutoff;
	
	public SpotLight(PointLight pointLight, Vector3f direction, float cutoff) {
		this.pointLight = pointLight;
		this.direction = direction.normalize();
		this.cutoff = cutoff;
	}

	public PointLight getPointLight() {
		return pointLight;
	}

	public void setPointLight(PointLight pointLight) {
		this.pointLight = pointLight;
	}

	public Vector3f getDirection() {
		return direction;
	}

	public void setDirection(Vector3f direction) {
		this.direction = direction.normalize();
	}

	public float getCutoff() {
		return cutoff;
	}

	public void setCutoff(float cutoff) {
		this.cutoff = cutoff;
	}
	
	
}
