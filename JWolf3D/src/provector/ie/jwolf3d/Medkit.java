package provector.ie.jwolf3d;

import provector.ie.jengine.Game;
import provector.ie.jengine.rendering.Mesh;
import provector.ie.jengine.rendering.Transform;
import provector.ie.jengine.rendering.entities.Material;
import provector.ie.jengine.rendering.entities.Texture;
import provector.ie.jengine.rendering.entities.Vector2f;
import provector.ie.jengine.rendering.entities.Vector3f;
import provector.ie.jengine.rendering.entities.Vertex;
import provector.ie.jengine.rendering.shaders.Shader;

public class Medkit {

	public static final float SCALE = 0.2f;
	public static final float SIZE_Y = SCALE;
	public static final float SIZE_X = (float)((double)SIZE_Y/(1.47)) ; //To calculate scale find out aspect ratio of the texture
	
	public static final float PICKUP_DISTANCE = 0.5f;
	public static final int HEAL_AMOUNT = 30;
	
	public static final float START = 0;
	
	public static final float OFFSET_X = 0.0f;
	public static final float OFFSET_Y = 0.0f;
	
	public static final float TEX_MIN_X = -OFFSET_X;
	public static final float TEX_MAX_X = 1 - OFFSET_X;
	public static final float TEX_MIN_Y = -OFFSET_Y;
	public static final float TEX_MAX_Y = 1 - OFFSET_Y;
	//private static final float OFFSET_FROM_GROUND = 0.00f; //Use -0.075f if loading with SlickUtil method
	
	private static Mesh mesh;
	private static Material material;
	private Transform transform;	
	
	public Medkit(Vector3f position) {
		
		if(mesh==null) {

			//TODO Note: add top and bottom faces for texture if lenghth<level height
			
			Vertex[] vertices = new Vertex[] {
					//Front
					new Vertex(new Vector3f(-SIZE_X, START,START),new Vector2f(TEX_MAX_X,TEX_MAX_X)), //0,0
					new Vertex(new Vector3f(-SIZE_X,SIZE_Y,START),new Vector2f(TEX_MAX_X,TEX_MIN_Y)),//0,-1
					new Vertex(new Vector3f(SIZE_X,SIZE_Y,START),new Vector2f(TEX_MIN_X,TEX_MIN_Y)),//-1,-1
					new Vertex(new Vector3f(SIZE_X, START,START),new Vector2f(TEX_MIN_X,TEX_MAX_Y)),//-1,0
					
					//TODO: add other planes?
			};
			
			int[] indices = new int[]{
					
					0,1,2,
					0,2,3,					
					
			};
			
			mesh = new Mesh(vertices,indices);
		}
		
		if(material==null) {
			material = new Material(new Texture("MEDIA0.png"));
		}
		
		transform = new Transform();
		transform.setTranslation(position);
	}
	
	public Vector3f faceCamera(Vector3f directionToCamera) {
		float angleToFaceTheCamera = (float)Math.toDegrees(Math.atan(directionToCamera.getZ()/directionToCamera.getX()));
		if(directionToCamera.getX()<0) {
			angleToFaceTheCamera+=180;
		}
		transform.getRotation().setY(angleToFaceTheCamera+90);
		return directionToCamera;
	}
	
	public void render() {
		Shader shader = Game.getLevel().getShader(); //TODO: check if really have to grab shader object every render
		shader.updateUniforms(transform.getTransformation(), transform.getProjectedTransformation(), material);
		mesh.draw();
	}	

	public void update() {
		
		Vector3f directionToCamera = faceCamera(Transform.getCamera().getPosition().sub(transform.getTranslation()));
		if(directionToCamera.length() < PICKUP_DISTANCE) {
			if(Game.getLevel().getPlayer().getHealth()<Player.MAX_HEALTH) {
				Game.getLevel().getPlayer().heal(HEAL_AMOUNT);
				Game.getLevel().removeMedkit(this); //TODO: think of better way of removing objects
			}
			
		}
	}
}
