package provector.ie.jwolf3d;

import provector.ie.jengine.components.Bitmap;

public class Debug {

	public static void printBitmapContents(Bitmap bitmap) {
		for(int i=0;i<bitmap.getWidth();i++) {
			for(int j=0;j<bitmap.getHeight();j++) {
				System.out.print(bitmap.getPixel(i, j)+",");
			}			
			System.out.println();
		}	
	}
}
