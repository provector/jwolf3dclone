package provector.ie.jwolf3d;

import java.util.ArrayList;

import provector.ie.jengine.Game;
import provector.ie.jengine.components.Bitmap;
import provector.ie.jengine.components.Util;
import provector.ie.jengine.rendering.Mesh;
import provector.ie.jengine.rendering.Transform;
import provector.ie.jengine.rendering.entities.Material;
import provector.ie.jengine.rendering.entities.Texture;
import provector.ie.jengine.rendering.entities.Vector2f;
import provector.ie.jengine.rendering.entities.Vector3f;
import provector.ie.jengine.rendering.entities.Vertex;
import provector.ie.jengine.rendering.shaders.BasicShader;
import provector.ie.jengine.rendering.shaders.Shader;

public class Level {
	
	private static final float SPOT_WIDTH = 1f;
	private static final float SPOT_LENGTH = 1f;
	private static final float SPOT_HEIGHT = 1f;
	
	private static final int NUM_TEX_EXPONENT = 4; //we have 16 textures sqrt/16
	private static final int NUM_TEXTURES = (int) Math.pow(2,NUM_TEX_EXPONENT);
	private static final Texture DOOR_TEXTURE = new Texture("Door_2_128.png");
	private static final float OPEN_DISTANCE = 1.0f;
	private static final float DOOR_OPEN_MOVEMENT_AMOUNT = 0.9f; //not 1 so the door won't go all the way in to the wall
	
	private Mesh mesh;
	private Bitmap level;
	private Shader shader;
	private Material levelMaterial;
	private Transform transform;
	private Material doorMaterial;
	private ArrayList<Door> doors;
	private ArrayList<Monster> monsters;
	private ArrayList<Medkit> medkits;
	private ArrayList<Medkit> medkitsToRemove;
	private Player player;
	
	private ArrayList<Vector2f> collisionPosStart;
	private ArrayList<Vector2f> collisionPosEnd;
	private ArrayList<Vector3f> exitPoints;
	
	
	public Level(String levelFile,String textureFile) {
			
			level = new Bitmap(levelFile).flipY();	
			//TODO: init player
			levelMaterial = new Material(new Texture(textureFile));
			doorMaterial = new Material(DOOR_TEXTURE);
		//Move those vars to generateLevel?
			shader = BasicShader.getInstance();						
			transform = new Transform();
			doors = new ArrayList<>();
			monsters = new ArrayList<>();
			medkits = new ArrayList<>();
			exitPoints = new ArrayList<>();
			medkitsToRemove = new ArrayList<>();
		//generate 2d collision mesh (for shoots collisions)
		collisionPosStart = new ArrayList<>();
		collisionPosEnd = new ArrayList<>();
		generateLevel();
		//TODO: Here add level checks?
		if(player == null) {
			System.out.println("No player position in map!");
			new Exception().printStackTrace();
			System.exit(1);
		}
		System.out.println("DEBUG: "+exitPoints.toString());			
	}
	
	public Vector3f checkCollisions(Vector3f oldPosition,Vector3f newPosition,float objectWidth_x,float obejctLength_z) {
		
		Vector2f collisionVector = new Vector2f(1,1);
		Vector3f movementVector = newPosition.sub(oldPosition);
		//Walls collisions
		if(movementVector.length()>0) {
			Vector2f blockSize = new Vector2f(SPOT_WIDTH,SPOT_HEIGHT);
			Vector2f objectSize = new Vector2f(objectWidth_x,obejctLength_z);
			
			Vector2f oldPosition2 = new Vector2f(oldPosition.getX(),oldPosition.getZ());
			Vector2f newPosition2 = new Vector2f(newPosition.getX(),newPosition.getZ());
			
			for (int i = 0; i < level.getWidth(); i++) {
				for (int j = 0; j < level.getHeight(); j++) {
					if((level.getPixel(i, j) & 0xFFFFFF) == 0 ) {
						collisionVector = collisionVector.mul(rectCollide(oldPosition2,newPosition2,objectSize,blockSize.mul(new Vector2f(i,j)),blockSize));
					}
				}
			}//endFor
			
			//Door collisions			
			for(Door door: doors) {
				Vector2f doorSize = door.getDoorSize();
				Vector3f doorPos3f = door.getTransform().getTranslation();
				Vector2f doorPos2f = new Vector2f(doorPos3f.getX(),doorPos3f.getZ());					
				collisionVector = collisionVector.mul(rectCollide(oldPosition2,newPosition2,objectSize,doorPos2f,doorSize));
			}
			
		}
		
		return new Vector3f(collisionVector.getX(),0,collisionVector.getY());
	}
	
	//TODO: copy this method separate for shooting from player (no boolean)
	public Vector2f checkIntersections(Vector2f lineStart, Vector2f lineEnd,boolean hurtMonsters) {
		
		Vector2f nearestIntersection = null;
		
		for(int i=0;i<collisionPosStart.size();i++) 
		{
			Vector2f collisionVector = lineIntersect(lineStart,lineEnd,collisionPosStart.get(i),collisionPosEnd.get(i));
			
			nearestIntersection = findNearestVector2f(nearestIntersection,collisionVector,lineStart);

		}
		
		for(Door door:doors) {
			
			Vector2f doorSize = door.getDoorSize();
			Vector3f doorPos3f = door.getTransform().getTranslation();
			Vector2f doorPos2f = new Vector2f(doorPos3f.getX(),doorPos3f.getZ());		
			
			Vector2f collisionVector = lineIntersectRect(lineStart,lineEnd,doorPos2f,doorSize);
			
			nearestIntersection = findNearestVector2f(nearestIntersection,collisionVector,lineStart);
		}
		
		if(hurtMonsters) {
			
			for(Monster monster:monsters) {
				
				Vector2f nearestMonsterIntersect = null;
				Monster nearestMonster = null;
				
				Vector2f monsterSize = monster.getSize();
				Vector3f monsterPos3f = monster.getTransform().getTranslation();
				Vector2f monsterPos2f = new Vector2f(monsterPos3f.getX(),monsterPos3f.getZ());		
				
				Vector2f collisionVector = lineIntersectRect(lineStart,lineEnd,monsterPos2f,monsterSize);
				
				//Vector2f lastMonsterIntersect = nearestMonsterIntersect;
				nearestMonsterIntersect = findNearestVector2f(nearestMonsterIntersect,collisionVector,lineStart);
				//update wchich monster is nearest
				if(nearestMonsterIntersect==collisionVector) {
					nearestMonster = monster;
				}
				
				if(nearestMonsterIntersect !=null && (nearestIntersection == null||nearestMonsterIntersect.sub(lineStart).length() < nearestIntersection.sub(lineStart).length())) {
					System.out.println("Monster hit!");
					if(nearestMonster!=null) { //TODO: check how works for dead monsters
						nearestMonster.damage(player.getDamage());
					}					
				}
				
			}
		}
		
		return nearestIntersection;
	}
	
	private float vector2fCross(Vector2f a,Vector2f b) {
		return a.getX() * b.getY() - a.getY() * b.getX();
	}
	
	//https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
	private Vector2f lineIntersect(Vector2f lineStart1,Vector2f lineEnd1,Vector2f lineStart2,Vector2f lineEnd2) {
		
		Vector2f line1 = lineEnd1.sub(lineStart1);
		Vector2f line2 = lineEnd2.sub(lineStart2);
		
		float cross = vector2fCross(line1,line2);
		if(cross==0) { //if sin is 0 they are parallel
			return null;
		}
			
		Vector2f distanceBetweenLineStarts = lineStart2.sub(lineStart1);
		float a = vector2fCross(distanceBetweenLineStarts,line2)/cross;
		float b = vector2fCross(distanceBetweenLineStarts,line1)/cross;
		
		if(0.0f < a && a < 1.0f && 0.0f < b && b < 1.0f) {
			return lineStart1.add(line1.mul(a));
		}
			
		return null;
	}
	
	private Vector2f rectCollide(Vector2f oldPosition,Vector2f newPosition,Vector2f firstObject,Vector2f position2,Vector2f secondObject) {
		
		Vector2f result = new Vector2f(0,0);
		if(newPosition.getX()+firstObject.getX()<position2.getX() || 
		   newPosition.getX()-firstObject.getX()>position2.getX() + secondObject.getX() * secondObject.getX() ||  //leftEdge > rightEdge
		   oldPosition.getY()+firstObject.getY()<position2.getY() |
		   oldPosition.getY()-firstObject.getY()>position2.getY() + secondObject.getY() * secondObject.getY()) {
			result.setX(1);
		}
		
		if(oldPosition.getX()+firstObject.getX()<position2.getX() || 
		   oldPosition.getX()-firstObject.getX()>position2.getX() + secondObject.getX() * secondObject.getX() ||  //leftEdge > rightEdge
		   newPosition.getY()+firstObject.getY()<position2.getY() |
		   newPosition.getY()-firstObject.getY()>position2.getY() + secondObject.getY() * secondObject.getY()) {
			result.setY(1);
		}
		
		return result;
	}
	
	public void openDoors(Vector3f position,boolean tryExit) {
		for(Door door:doors) {
			if(door.getTransform().getTranslation().sub(position).length() < OPEN_DISTANCE) {
				door.open();
			}	
			
		}//endFor
		
		if(tryExit) {
			for(Vector3f exitPoint : exitPoints) {
				if(exitPoint.sub(position).length() < OPEN_DISTANCE) {
					Game.loadNextLevel();
					break;
				}
			}
		}
	}
	
	public void input() {
		
		
		
		player.input();
	}
	
	public void damagePlayer(int amount) {
		player.damage(amount);
	}
	
	public void update() {
		
		for(Door door: doors) {
			door.update();
		}
		
		player.update();
		
		for(Monster monster: monsters) {
			monster.update();
		}
		

		for(Medkit medkit:medkitsToRemove) {
			medkits.remove(medkit);
		}
		
		for(Medkit medkit:medkits) {
			medkit.update();
		}
		
		
	
	}
	
	public void render() {
		
		shader.bind(); //TODO: this should be done only once at init I think
		shader.updateUniforms(transform.getTransformation(),transform.getProjectedTransformation(), levelMaterial);
		mesh.draw();
		
		for(Door door: doors) {
			door.render();
		}
		//draw monsters before players for transparency order (gun overwriting sprites)
		for(Monster monster:monsters) {
			monster.render();
		}
		
		for(Medkit medkit:medkits) {
			medkit.render();
		}
		
		player.render();
		
		
		
	}
	
	private void addFace(ArrayList<Integer> indices,int startLocation,boolean direction) {
		if(direction) {
			indices.add(startLocation+2);
			indices.add(startLocation+1);
			indices.add(startLocation+0);
			
			indices.add(startLocation+3);
			indices.add(startLocation+2);
			indices.add(startLocation+0);				
		}else {
			indices.add(startLocation+0);
			indices.add(startLocation+1);
			indices.add(startLocation+2);
			
			indices.add(startLocation+0);
			indices.add(startLocation+2);
			indices.add(startLocation+3);			
		}
	}
	
	private float[] calcTexCoords(int baseColorShift) {
		int texX = baseColorShift/NUM_TEXTURES;
		int texY = texX % NUM_TEX_EXPONENT;
		texX /= NUM_TEX_EXPONENT;
						
		float[] result = new float[4];
		
		result[0] = 1f-(float)texX/(float)NUM_TEX_EXPONENT;
		result[1] = result[0]-1/(float)NUM_TEX_EXPONENT;
		result[3] = 1f-(float)texY/(float)NUM_TEX_EXPONENT;
		result[2] = result[3]-1/(float)NUM_TEX_EXPONENT;
		
		return result;
	}
	
	private void addVertices(ArrayList<Vertex> vertices,int i,int j,float offset,boolean x,boolean y,boolean z,float[] texCoords) {
		
		/*
		 			float xHigher = texCoords[0];
					float xLower = texCoords[1];
					float yHigher = texCoords[2];
					float yLower = texCoords[3];
		 */
		
		if(x && z ) {
			vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, offset*SPOT_HEIGHT,   j *SPOT_LENGTH),new Vector2f(texCoords[1],texCoords[3])));
			vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, offset*SPOT_HEIGHT,   j *SPOT_LENGTH),new Vector2f(texCoords[0],texCoords[3])));
			vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, offset*SPOT_HEIGHT,(j+1)*SPOT_LENGTH),new Vector2f(texCoords[0],texCoords[2])));
			vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, offset*SPOT_HEIGHT,(j+1)*SPOT_LENGTH),new Vector2f(texCoords[1],texCoords[2])));
		}else if(x && y) {
			vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH,     j*SPOT_HEIGHT,offset*SPOT_LENGTH),new Vector2f(texCoords[1],texCoords[3])));
			vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH,     j*SPOT_HEIGHT,offset*SPOT_LENGTH),new Vector2f(texCoords[0],texCoords[3])));
			vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, (j+1)*SPOT_HEIGHT,offset*SPOT_LENGTH),new Vector2f(texCoords[0],texCoords[2])));
			vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, (j+1)*SPOT_HEIGHT,offset*SPOT_LENGTH),new Vector2f(texCoords[1],texCoords[2])));
		}else if(y && z) {
			vertices.add(new Vertex(new Vector3f(offset*SPOT_WIDTH,	   i*SPOT_HEIGHT,    j*SPOT_LENGTH),new Vector2f(texCoords[1],texCoords[3])));
			vertices.add(new Vertex(new Vector3f(offset*SPOT_WIDTH,    i*SPOT_HEIGHT,(j+1)*SPOT_LENGTH),new Vector2f(texCoords[0],texCoords[3])));
			vertices.add(new Vertex(new Vector3f(offset*SPOT_WIDTH,(i+1)*SPOT_HEIGHT,(j+1)*SPOT_LENGTH),new Vector2f(texCoords[0],texCoords[2])));
			vertices.add(new Vertex(new Vector3f(offset*SPOT_WIDTH,(i+1)*SPOT_HEIGHT,    j*SPOT_LENGTH),new Vector2f(texCoords[1],texCoords[2])));
		}else {
			System.out.println("Something went terribly wrong: Invalid plane in level generaton");
			new Exception().printStackTrace();
			System.exit(1);
		}
	}
	
	private void addSpecial(int blueValue,int x,int y) {
		if(blueValue==16) {
			addDoor(x,y);
		}
		if(blueValue==1) {
			player = new Player(new Vector3f((x+0.5f)*SPOT_WIDTH,Player.PLAYER_GROUND_OFFSET,(y+0.5f)*SPOT_LENGTH));//0.5f center of the tile
		}
		if(blueValue==128) {
			
			Transform monsterPosition = new Transform();
			monsterPosition.setTranslation(new Vector3f((x+0.5f)*SPOT_WIDTH,0,(y+0.5f)*SPOT_LENGTH));
			monsters.add(new Monster(monsterPosition));
		}
		if(blueValue==192) {
			
			medkits.add(new Medkit(new Vector3f((x+0.5f)*SPOT_WIDTH,0,(y+0.5f)*SPOT_LENGTH)));
		}
		if(blueValue==97) {
			exitPoints.add(new Vector3f((x+0.05f)*SPOT_WIDTH,0,(y+0.05f)*SPOT_LENGTH));
		}
	}
	
	private void addDoor(int x,int y) {
		//TODO: probably can re-write this better
		Transform doorTransform =  new Transform();
		
		//detect proper axis;
		boolean xDoor = ((level.getPixel(x, y-1) & 0xFFFFFF) == 0) && ((level.getPixel(x, y+1) & 0xFFFFFF) == 0);
		boolean yDoor = ((level.getPixel(x-1, y) & 0xFFFFFF) == 0) && ((level.getPixel(x+1, y) & 0xFFFFFF) == 0); //That line is probably redundant

		if((xDoor^yDoor)==false) {
			System.out.println("Level generation error. Invalid door location ("+x+","+y+")");
			new Exception().printStackTrace();
			System.exit(1);
		}
		
		Vector3f openPosition = null;
		
		if(yDoor) {
			doorTransform.setTranslation(x,0,y+SPOT_LENGTH/2); //y+SPOT_LENGTH/2;
			openPosition = doorTransform.getTranslation().sub(new Vector3f(DOOR_OPEN_MOVEMENT_AMOUNT,0,0));
		}
		if(xDoor) {
			doorTransform.setTranslation(x+SPOT_WIDTH/2,0,y); //x+SPOT_WIDTH/2
			doorTransform.setRotation(0,90,0);
			openPosition = doorTransform.getTranslation().sub(new Vector3f(0,0,DOOR_OPEN_MOVEMENT_AMOUNT));
		}
		
		doors.add(new Door(doorTransform,doorMaterial,openPosition));
	}
	
	private void generateLevel() {		//Generate Level
		ArrayList<Vertex> vertices = new ArrayList<>();
		ArrayList<Integer> indices = new ArrayList<>();
				
		for(int i=0;i<level.getWidth();i++) {
			for(int j=0;j<level.getHeight();j++) 
			{
				if((level.getPixel(i, j)&0xFFFFFF)==0) {
					continue; //if black skip
				}
				
					
				/*
				 * So the color component we are shifting by is a coresponding component in gleditor (r,g,b);
				 * In example, ceiling ix 0x00FF00 which is green, green color slider value will determin floor/ceiling texture.
				 * So if we want texture 11 from matrix (counted bottom up then left from bottom right corner) we do 11*16 = 176
				 * and set green component to 176. Calculate the same for red component (walls) and fill the level file with appropriate color
				 */
				
				//Floor
				
					//Texture Coords
					float[] texCoords = calcTexCoords((level.getPixel(i, j) & 0x00FF00)>>8);	
					
					addSpecial((level.getPixel(i, j) & 0x0000FF),i,j);
					
					//texture faces
					addFace(indices,vertices.size(),true);
					
					//vertices
					addVertices(vertices,i,j,0,true,false,true,texCoords);
					
				//Ceiling					
						
					//texture faces
					addFace(indices,vertices.size(),false);
					
					//vertices
					addVertices(vertices,i,j,1,true,false,true,texCoords);			
				
				//Walls
					
					//Texture 
					texCoords = calcTexCoords((level.getPixel(i, j) & 0xFF0000)>>16);								
					
					//Z axis
					if((level.getPixel(i, j-1) &0xFFFFFF)==0) { //check if adjacent pixel is black
						//generate 2d collision mesh
						collisionPosStart.add(new Vector2f(i*SPOT_WIDTH,j*SPOT_LENGTH));
						collisionPosEnd.add(new Vector2f((i+1)*SPOT_WIDTH,j*SPOT_LENGTH));

						//texture faces
						addFace(indices,vertices.size(),false);				
						
						//vertices
						addVertices(vertices,i,0,j,true,true,false,texCoords);	
					}
					
					//other direction
					if((level.getPixel(i, j+1) &0xFFFFFF)==0) { //check if adjacent pixel is black
						//generate 2d collision mesh
						collisionPosStart.add(new Vector2f(i*SPOT_WIDTH,(j+1)*SPOT_LENGTH));
						collisionPosEnd.add(new Vector2f((i+1)*SPOT_WIDTH,(j+1)*SPOT_LENGTH));
						
						//texture faces
						addFace(indices,vertices.size(),true);		
						
						//vertices
						addVertices(vertices,i,0,(j+1),true,true,false,texCoords);			
					}
					
					//Xaxis
					if((level.getPixel(i-1, j) &0xFFFFFF)==0) { //check if adjacent pixel is black
						//generate 2d collision mesh
						collisionPosStart.add(new Vector2f(i*SPOT_WIDTH,j*SPOT_LENGTH));
						collisionPosEnd.add(new Vector2f(i*SPOT_WIDTH,(j+1)*SPOT_LENGTH));
						
						//texture faces
						addFace(indices,vertices.size(),true);				
						
						//vertices
						addVertices(vertices,0,j,i,false,true,true,texCoords);	
					}
					
					//Xaxis - other direction
					if((level.getPixel(i+1, j) &0xFFFFFF)==0) { //check if adjacent pixel is black
						//generate 2d collision mesh
						collisionPosStart.add(new Vector2f((i+1)*SPOT_WIDTH,j*SPOT_LENGTH));
						collisionPosEnd.add(new Vector2f((i+1)*SPOT_WIDTH,(j+1)*SPOT_LENGTH));
						
						//texture faces
						addFace(indices,vertices.size(),false);
						
						//vertices
						addVertices(vertices,0,j,(i+1),false,true,true,texCoords);			

					}
					
			}
		}
		
		Vertex[] vertArray = new Vertex[vertices.size()];
		Integer[] intArray = new Integer[indices.size()];
		
		vertices.toArray(vertArray);
		indices.toArray(intArray);
		
		mesh = new Mesh(vertArray,Util.toIntArray(intArray),false);;
	}
	
	public Shader getShader() {
		return this.shader;
	}

	public Player getPlayer() {
		return this.player;
	}
	
	private Vector2f findNearestVector2f(Vector2f a,Vector2f b,Vector2f positionRelativeTo) {
		if(b!=null && (a == null || a.sub(positionRelativeTo).length() > b.sub(positionRelativeTo).length())) {
			return b;		
		}else {
			return a;
		}
	}
	
	public Vector2f lineIntersectRect(Vector2f lineStart, Vector2f lineEnd, Vector2f rectPos, Vector2f rectSize) {
		
		Vector2f result = null;
		
		Vector2f collisionVector = lineIntersect(lineStart,lineEnd,rectPos,new Vector2f(rectPos.getX()+rectSize.getX(),rectPos.getY()));
		result = findNearestVector2f(result,collisionVector,lineStart);
		
		collisionVector = lineIntersect(lineStart,lineEnd,rectPos,new Vector2f(rectPos.getX(),rectPos.getY()+rectSize.getY()));
		result = findNearestVector2f(result,collisionVector,lineStart);
		
		collisionVector = lineIntersect(lineStart,lineEnd,new Vector2f(rectPos.getX(),rectPos.getY()+rectSize.getY()),rectPos.add(rectSize));
		result = findNearestVector2f(result,collisionVector,lineStart);
		
		collisionVector = lineIntersect(lineStart,lineEnd,new Vector2f(rectPos.getX()+rectSize.getX(),rectPos.getY()),rectPos.add(rectSize));
		result = findNearestVector2f(result,collisionVector,lineStart);
		
		return result;
	}

	
	public void removeMedkit(Medkit medkit) {
		medkitsToRemove.add(medkit);
	}
	
}
