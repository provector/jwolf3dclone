package provector.ie.jwolf3d;

import provector.ie.jengine.Game;
import provector.ie.jengine.components.Sound;
import provector.ie.jengine.components.Time;
import provector.ie.jengine.rendering.Mesh;
import provector.ie.jengine.rendering.Transform;
import provector.ie.jengine.rendering.entities.Material;
import provector.ie.jengine.rendering.entities.Vector2f;
import provector.ie.jengine.rendering.entities.Vector3f;
import provector.ie.jengine.rendering.entities.Vertex;
import provector.ie.jengine.rendering.shaders.Shader;

public class Door {

	public static final float LENGTH = 1;
	public static final float HEIGHT = 1;
	public static final float WIDTH = 0.125f;
	public static final float START = 0;
	
	private static Mesh mesh;
	private Transform transform;
	private Material material;
	
	//Door openning
	public final static double TIME_TO_OPEN = 1.0; //TODO: adjust values
	public final static double CLOSE_DELAY = 2.0;
	private boolean isOpenning;
	private double openningStartTime;
	private double closingStartTime;
	private double opennedTime;
	private double closedTime;
	
	private Vector3f openPosition;
	private Vector3f closePosition;
	
	public Door(Transform transform,Material material,Vector3f openPosition) {
		
		this.transform = transform;
		this.material = material;
		this.isOpenning = false;
		this.closePosition = transform.getTranslation().mul(1);
		this.openPosition = openPosition;
		if(mesh==null) {

			//TODO Note: add top and bottom faces for texture if lenghth<level height
			
			Vertex[] vertices = new Vertex[] {
					//Front
					new Vertex(new Vector3f( START, START,START),new Vector2f(0,0)),
					new Vertex(new Vector3f( START,HEIGHT,START),new Vector2f(0,-1)),
					new Vertex(new Vector3f(LENGTH,HEIGHT,START),new Vector2f(-1,-1)),
					new Vertex(new Vector3f(LENGTH, START,START),new Vector2f(-1,0)),
					
					//Left
					new Vertex(new Vector3f(START, START,START),new Vector2f(0,0)),
					new Vertex(new Vector3f(START,HEIGHT,START),new Vector2f(1,0f)),
					new Vertex(new Vector3f(START,HEIGHT,WIDTH),new Vector2f(1,0.085f)),
					new Vertex(new Vector3f(START, START,WIDTH),new Vector2f(0,0.085f)),
					
					//Back
					new Vertex(new Vector3f( START, START,WIDTH),new Vector2f(0,0)),
					new Vertex(new Vector3f( START,HEIGHT,WIDTH),new Vector2f(0,-1)),
					new Vertex(new Vector3f(LENGTH,HEIGHT,WIDTH),new Vector2f(-1,-1)),
					new Vertex(new Vector3f(LENGTH, START,WIDTH),new Vector2f(-1,0)),
					
					new Vertex(new Vector3f(LENGTH, START,START),new Vector2f(0,0)),
					new Vertex(new Vector3f(LENGTH,HEIGHT,START),new Vector2f(1,0)),
					new Vertex(new Vector3f(LENGTH,HEIGHT,WIDTH),new Vector2f(1,0.085f)),
					new Vertex(new Vector3f(LENGTH, START,WIDTH),new Vector2f(0,0.085f))
			};
			
			int[] indices = new int[]{
					
					0,1,2,
					0,2,3,
					
					6,5,4,
					7,6,4,
					
					10,9,8,
					11,10,8,
					
					12,13,14,
					12,14,15
			};
			
			mesh = new Mesh(vertices,indices);
		}
	}
	
	private Vector3f vectorLERP(Vector3f startPos,Vector3f endPos,float lerpFactor) { //Linear Interpolation
		return startPos.add(endPos.sub(startPos).mul(lerpFactor));		
	}
	
	public void open() {
		if(isOpenning) return;
		this.isOpenning = true;
		openningStartTime = (double)Time.getTime()/(double)Time.SECOND;
		opennedTime = openningStartTime + TIME_TO_OPEN;
		closingStartTime = opennedTime+CLOSE_DELAY;
		closedTime = closingStartTime + TIME_TO_OPEN;
		Sound.playClip("Door.wav");

	}
	
	
	
	public void update() {
		//TOOD: base for animation?
		if(isOpenning) {
			//check keyframes
			double time = (double)Time.getTime()/(double)Time.SECOND;
			if(time<opennedTime) { //Start Openning door
				float lerpFactor = (float)((time - openningStartTime)/TIME_TO_OPEN);
				getTransform().setTranslation(vectorLERP(closePosition,openPosition,lerpFactor));
			}else if(time<closingStartTime){ //Keep door open
				getTransform().setTranslation(openPosition);
			}else if(time<closedTime) {//Start closing door
				float lerpFactor = (float)((time - closingStartTime)/TIME_TO_OPEN);
				getTransform().setTranslation(vectorLERP(openPosition,closePosition,lerpFactor));
			}else {//Doors closed
				getTransform().setTranslation(closePosition);
				isOpenning = false;
			}
		}
	}
	
	public void render() {
		
		Shader shader = Game.getLevel().getShader();
		shader.updateUniforms(transform.getTransformation(),transform.getProjectedTransformation(), material);
		mesh.draw();
	}
	
	public Vector2f getDoorSize() {
		if(getTransform().getRotation().getY() == 90 ) {
			return new Vector2f(Door.WIDTH,Door.LENGTH);
		}else {
			return new Vector2f(Door.LENGTH,Door.WIDTH);
		}
		
	}
	
	public Transform getTransform() {
		return transform;
	}
}

