package provector.ie.jwolf3d;

import java.util.ArrayList;
import java.util.Random;

import provector.ie.jengine.Game;
import provector.ie.jengine.components.Sound;
import provector.ie.jengine.components.Time;
import provector.ie.jengine.rendering.Mesh;
import provector.ie.jengine.rendering.Transform;
import provector.ie.jengine.rendering.entities.Material;
import provector.ie.jengine.rendering.entities.Texture;
import provector.ie.jengine.rendering.entities.Vector2f;
import provector.ie.jengine.rendering.entities.Vector3f;
import provector.ie.jengine.rendering.entities.Vertex;
import provector.ie.jengine.rendering.shaders.Shader;

public class Monster {
	
	//TODO: invisible to camera at init
	
	public static final float SCALE = 0.7f;
	public static final float SIZE_Y = SCALE;
	public static final float SIZE_X = (float)((double)SIZE_Y/(1.9310344827586206896551724137931*2.0)) ;
	public static final float WIDTH = 0.125f;
	public static final float START = 0;
		
	public static final float OFFSET_X = 0.0f;
	public static final float OFFSET_Y = 0.0f;
	
	public static final float TEX_MIN_X = -OFFSET_X;
	public static final float TEX_MAX_X = 1 - OFFSET_X;
	public static final float TEX_MIN_Y = -OFFSET_Y;
	public static final float TEX_MAX_Y = 1 - OFFSET_Y;
	private static final float OFFSET_FROM_GROUND = 0.00f; //Use -0.075f if loading with SlickUtil method
	
	public STATE state;	
	private boolean canLook;
	private boolean canAttack;
	
	public static final float MOVE_SPEED = 2; //TODO: adjust
	public static final float MOVE_STOP_DISTANCE = 1;
	public static final float MONSTER_WIDTH = 0.2f;
	public static final float MONSTER_LENGTH = 0.2f;
	public static final float SHOOT_RANGE = 1000;
	public static final float SHOT_ANGLE = 10; //degrees
	public static final int MAX_HEALTH = 100;
	public static final int DAMAGE_MIN = 5;
	public static final int DAMAGE_MAX = 30;
	
	private static final float ATTACK_CHANCE = 0.5f;
		
	private Transform transform;
	private Mesh mesh;
	private ArrayList<Texture> animations;
	private Material material;
	private Random rand;
	private int health;
	
	private double deathTime;
	final static float D_FRAME_DTIME_1 = 0.1f; //death_frame_decimal_time
	final static float D_FRAME_DTIME_2 = 0.3f;
	final static float D_FRAME_DTIME_3 = 0.45f;
	final static float D_FRAME_DTIME_4 = 0.6f;
	
	//private static Shader shader = Game.getLevel().getShader();  //originally this was in render method

	public Monster(Transform transform) {
		
		//material = new Material(new Texture("SSWVA1.png"));
		if(animations==null) {
			animations = new ArrayList<>();
			//walking
			animations.add(new Texture("SSWVA1.png"));
			animations.add(new Texture("SSWVB1.png"));
			animations.add(new Texture("SSWVC1.png"));
			animations.add(new Texture("SSWVD1.png"));
			//shooting
			animations.add(new Texture("SSWVE0.png"));			
			animations.add(new Texture("SSWVF0.png"));
			animations.add(new Texture("SSWVG0.png"));
			//pain
			animations.add(new Texture("SSWVH0.png"));
			//dying
			animations.add(new Texture("SSWVI0.png"));
			animations.add(new Texture("SSWVJ0.png"));
			animations.add(new Texture("SSWVK0.png"));
			animations.add(new Texture("SSWVL0.png"));
			//dead
			animations.add(new Texture("SSWVM0.png"));		
		}
		
		if(mesh==null) {

			//TODO Note: add top and bottom faces for texture if lenghth<level height
			
			Vertex[] vertices = new Vertex[] {
					//Front
					new Vertex(new Vector3f(-SIZE_X, START,START),new Vector2f(TEX_MAX_X,TEX_MAX_X)), //0,0
					new Vertex(new Vector3f(-SIZE_X,SIZE_Y,START),new Vector2f(TEX_MAX_X,TEX_MIN_Y)),//0,-1
					new Vertex(new Vector3f(SIZE_X,SIZE_Y,START),new Vector2f(TEX_MIN_X,TEX_MIN_Y)),//-1,-1
					new Vertex(new Vector3f(SIZE_X, START,START),new Vector2f(TEX_MIN_X,TEX_MAX_Y)),//-1,0
					
					//TODO: add other planes?
			};
			
			int[] indices = new int[]{
					
					0,1,2,
					0,2,3,					
					
			};
			
			mesh = new Mesh(vertices,indices);
		}
		
		this.transform = transform;
		this.canLook = false;
		this.canAttack = false;
		this.state = STATE.IDLE;
		this.rand = new Random();
		this.health = MAX_HEALTH;
		this.material = new Material(animations.get(0));
		this.deathTime = 0;
	}
	
	public Transform getTransform() {
		return this.transform;
	}
	
	private void alignWithGround() {
		transform.getTranslation().setY(OFFSET_FROM_GROUND);
	}
	
	private void faceCamera(Vector3f directionToCamera) {
		float angleToFaceTheCamera = (float)Math.toDegrees(Math.atan(directionToCamera.getZ()/directionToCamera.getX()));
		if(directionToCamera.getX()<0) {
			angleToFaceTheCamera+=180;
		}
		transform.getRotation().setY(angleToFaceTheCamera+90);
	}
	
	public void update() {
		
		Vector3f directionToCamera = Transform.getCamera().getPosition().sub(transform.getTranslation());

		float distance = directionToCamera.length();
		Vector3f orientation = directionToCamera.div(distance);
		
		alignWithGround();
		//make sprite always face the player	
		faceCamera(orientation);
		
		switch(state) {
			case IDLE:  idleUpdate(orientation,distance);
						break;
			case CHASE: chaseUpdate(orientation,distance);
						break;
			case ATTACK:attackUpdate(orientation,distance);
						break;
			case DYING: dyingUpdate(orientation,distance);
						break;
			case DEAD:  deadUpdate(orientation,distance);
						break;
			
			default:
		}
	}
	
	private void idleUpdate(Vector3f orientation,float distance) {
		double time = Time.getTime()/(double)Time.SECOND;
		double timeDecimals = time -(double)((int)time);
		
		if(timeDecimals < 0.1) //checks once a second for performance 
		{
			canLook = true;
		}else
		if(timeDecimals < 0.5) //different timing for animation
		{
			material.setTexture(animations.get(0));
		}
		else 
		{
			material.setTexture(animations.get(1));
			
			if(canLook) 
			{
				Vector2f lineStart = new Vector2f(transform.getTranslation().getX(),transform.getTranslation().getZ());
				Vector2f castDirection = new Vector2f(orientation.getX(),orientation.getZ());
				//we assume here camera will be the player
				Vector2f lineEnd = lineStart.add(castDirection.mul(SHOOT_RANGE));
				Vector2f collisionVector = Game.getLevel().checkIntersections(lineStart,lineEnd,false);
				Vector2f playerIntersectVector = new Vector2f(Transform.getCamera().getPosition().getX(),Transform.getCamera().getPosition().getZ());
						//Game.getLevel().lineIntersectRect(lineStart,lineEnd,
						//								new Vector2f(Transform.getCamera().getPosition().getX(),Transform.getCamera().getPosition().getZ()),
						//								new Vector2f(Player.PLAYER_SIZE,Player.PLAYER_SIZE));
					
				//TODO: use this as additional check to shoot player only when in sight (derive to separate method, player spotted?)
				if(playerIntersectVector !=null && (collisionVector == null||playerIntersectVector.sub(lineStart).length() < collisionVector.sub(lineStart).length())) {
					System.out.println("Player spotted");
					Sound.playClip("Halten_Sie!.wav");
					state = STATE.CHASE;
				}
				
				canLook = false;
			}
		}
		
		
	}
	
	private void chaseUpdate(Vector3f orientation,float distance) {
		
		double time = Time.getTime()/(double)Time.SECOND;
		double timeDecimals = time -(double)((int)time);
		//TODO: this could be probably automated...
		if(timeDecimals < 0.25)  //animation
		{
			material.setTexture(animations.get(0));
		}else
		if(timeDecimals < 0.5)
		{
			material.setTexture(animations.get(1));
		}else
		if(timeDecimals < 0.75) {
			material.setTexture(animations.get(2));
		}else 
		{
			material.setTexture(animations.get(3));
		}		
		
		if(rand.nextDouble()<ATTACK_CHANCE * Time.getDelta()) {
			state = STATE.ATTACK;
		}
		
		//following that can implement close range combat
		if(distance>MOVE_STOP_DISTANCE) {
			
			float moveAmount = MOVE_SPEED*(float)Time.getDelta();

			Vector3f oldPos = transform.getTranslation();
			Vector3f newPos = transform.getTranslation().add(orientation.mul(moveAmount));
			
			Vector3f collisionVector = Game.getLevel().checkCollisions(oldPos,newPos,MONSTER_WIDTH,MONSTER_LENGTH);
			Vector3f movementVector = collisionVector.mul(orientation);
									
			if(movementVector.length()>0) {
				transform.setTranslation(transform.getTranslation().add(movementVector.mul(moveAmount)));
			}
			
			//make it open doors
			if(movementVector.sub(orientation).length()!=0) {
				Game.getLevel().openDoors(transform.getTranslation(),false);
			}
			
		}
		else {
			state = STATE.ATTACK;
		}
	}
	
	private void attackUpdate(Vector3f orientation,float distance) {
		
		double time = Time.getTime()/(double)Time.SECOND;
		double timeDecimals = time -(double)((int)time);
		
//		if(timeDecimals < 0.1) //checks once a second for performance 
//		{
//			//TODO: canAttack was here
//		}else
		if(timeDecimals < 0.25) {
			material.setTexture(animations.get(4));
		}else
		if(timeDecimals < 0.5)
		{
			material.setTexture(animations.get(5));
		}else
		if(timeDecimals < 0.75)
		{	 
			material.setTexture(animations.get(6));
			//TODO: that can be refactored as well to handle animations in separate step
			 if(canAttack) {
				 Vector2f lineStart = new Vector2f(transform.getTranslation().getX(),transform.getTranslation().getZ());
					Vector2f castDirection = new Vector2f(orientation.getX(),orientation.getZ()).rotate((rand.nextFloat()-0.5f)*SHOT_ANGLE); //randomize accuracy
					//we assume here camera will be the player
					Vector2f lineEnd = lineStart.add(castDirection.mul(SHOOT_RANGE));
					Vector2f collisionVector = Game.getLevel().checkIntersections(lineStart,lineEnd,false);
					Vector2f playerIntersectVector = Game.getLevel().lineIntersectRect(lineStart,lineEnd,
															new Vector2f(Transform.getCamera().getPosition().getX(),Transform.getCamera().getPosition().getZ()),
															new Vector2f(Player.PLAYER_SIZE,Player.PLAYER_SIZE));
					
					if(playerIntersectVector !=null && (collisionVector == null||playerIntersectVector.sub(lineStart).length() < collisionVector.sub(lineStart).length())) {
						System.out.println("player hit");
						Game.getLevel().damagePlayer(getDamage());
					}
					
					if(collisionVector==null) {
						System.out.println("Missed everything");
					}else {
						System.out.println("hit wall");
					}
					state = STATE.CHASE;
					canAttack = false;
			 }			
		}else 
		{
			material.setTexture(animations.get(5));
			canAttack = true;
		}
		
		
	}
	
	public int getDamage() {
		return rand.nextInt(DAMAGE_MAX-DAMAGE_MIN)+DAMAGE_MIN;
	}
	
	public void damage(int amount) {
		
		if(state==STATE.IDLE) {
			state = STATE.CHASE;
		}
		health-=amount;
		System.out.println("Monster health: "+health);
		if(health<=0) {
			state = STATE.DYING;
			Sound.playClip("Death_1.wav");
		}
	}
	
	private void dyingUpdate(Vector3f orientation,float distance) {
		double time = Time.getTime()/(double)Time.SECOND;
		//double timeDecimals = time -(double)((int)time);
		
		if(deathTime == 0) {
			deathTime = time;
		}
		
		//TODO: sometimes have to figure out anim timing (again, can it be automated?)
		//also scale values are a bit off
		if(time < deathTime + D_FRAME_DTIME_1) {
			material.setTexture(animations.get(8));
			transform.setScale(1,0.96f,1);
		}else
		if(time < deathTime + D_FRAME_DTIME_2) {
			material.setTexture(animations.get(9));
			transform.setScale(1.7f,0.9f,1);
		}else
		if(time < deathTime + D_FRAME_DTIME_3) {
			material.setTexture(animations.get(10));
			transform.setScale(1.7f,0.9f,1);
		}else
		if(time < deathTime + D_FRAME_DTIME_4) {
			material.setTexture(animations.get(11));	
			transform.setScale(1.7f,0.5f,1);
		}else {
			state = STATE.DEAD;
		}		
	}
	
	private void deadUpdate(Vector3f orientation,float distance) {
		
		material.setTexture(animations.get(12));
		transform.setScale(1.76f,0.28f,1);
		//TODO: remove dead monster from list or make uneffective (Still can shoot it)
	}
	
	public void render() {
		
		Shader shader = Game.getLevel().getShader(); //TODO: check if really have to grab shader object every render
		shader.updateUniforms(transform.getTransformation(), transform.getProjectedTransformation(), material);
		mesh.draw();
	}
	
	public enum STATE {
		IDLE,CHASE,ATTACK,DYING,DEAD
	}

	public Vector2f getSize() {
		return new Vector2f(MONSTER_WIDTH,MONSTER_LENGTH);
	}
}
;