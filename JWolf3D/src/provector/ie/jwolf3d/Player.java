package provector.ie.jwolf3d;

import java.util.ArrayList;
import java.util.Random;

import provector.ie.jengine.Game;
import provector.ie.jengine.components.Input;
import provector.ie.jengine.components.Sound;
import provector.ie.jengine.components.Time;
import provector.ie.jengine.components.Window;
import provector.ie.jengine.rendering.Camera;
import provector.ie.jengine.rendering.Mesh;
import provector.ie.jengine.rendering.Transform;
import provector.ie.jengine.rendering.entities.Material;
import provector.ie.jengine.rendering.entities.Texture;
import provector.ie.jengine.rendering.entities.Vector2f;
import provector.ie.jengine.rendering.entities.Vector3f;
import provector.ie.jengine.rendering.entities.Vertex;
import provector.ie.jengine.rendering.shaders.Shader;

public class Player {
	
	private static final float MOUSE_SENSITIVITY = 0.1f;
	private static final float MOVE_SPEED = 6f;
	public static final float PLAYER_SIZE = 0.2f;
	private static final Vector3f ZERO_VECTOR = new Vector3f(0,0,0);
	public static final float SHOOT_DISTANCE = 1000;
	
	//TODO: that can be done better
	public static int DAMAGE_MIN = 33;
	public static int DAMAGE_MAX = 37;
	public static int MAX_HEALTH = 100;
	
	//Gun
	public static final float SCALE = 0.08f;
	public static final float SIZE_Y = SCALE;
	public static final float SIZE_X = (float)((double)SIZE_Y/(1.04*2.0)) ;
	public static final float START = 0;
	public static final float PLAYER_GROUND_OFFSET = 0.4375f;

	public static final float OFFSET_X = 0.0f;
	public static final float OFFSET_Y = 0.0f;
	
	public static final float TEX_MIN_X = -OFFSET_X;
	public static final float TEX_MAX_X = 1 - OFFSET_X;
	public static final float TEX_MIN_Y = -OFFSET_Y;
	public static final float TEX_MAX_Y = 1 - OFFSET_Y;
	
	private static final float GUN_CAMERA_VECTOR_CONSTANT = 0.105f;
	public static final float GUN_OFFSET = -0.08f;
	
	private static final float SHOOT_FRAME_T1 = 0.1f;
	private static final float SHOOT_FRAME_T2 = 0.2f;
	private static final float SHOOT_FRAME_T3 = 0.3f;
	private static final float SHOOT_FRAME_T4 = 0.4f;

	private static boolean canShoot;
	private static boolean playShootAnimation;
	private static double shootTime;
	
	private Random rand;
	
	private Camera camera;
	private int health;
	private static boolean mouseLocked = false;
	private Vector2f centerPosition = new Vector2f(Window.getWidth()/2,Window.getHeight()/2);
	private Vector3f movementVector;
	
	private static Mesh gunMesh;
	private static Material gunMaterial;
	private Transform gunTransform;
	
	private ArrayList<Texture> gunAnimation;

	public Player(Vector3f position) {
		
		shootTime = 0;
		canShoot = true;
		playShootAnimation = false;
		
		gunAnimation = new ArrayList<>();
		
		gunAnimation.add(new Texture("GUN0.png"));
		gunAnimation.add(new Texture("GUN1.png"));
		gunAnimation.add(new Texture("GUN2.png"));
		
		if(gunMesh==null) {

			//TODO Note: add top and bottom faces for texture if lenghth<level height
			
			Vertex[] vertices = new Vertex[] {
					//Front
					new Vertex(new Vector3f(-SIZE_X, START,START),new Vector2f(TEX_MAX_X,TEX_MAX_X)), //0,0
					new Vertex(new Vector3f(-SIZE_X,SIZE_Y,START),new Vector2f(TEX_MAX_X,TEX_MIN_Y)),//0,-1
					new Vertex(new Vector3f(SIZE_X,SIZE_Y,START),new Vector2f(TEX_MIN_X,TEX_MIN_Y)),//-1,-1
					new Vertex(new Vector3f(SIZE_X, START,START),new Vector2f(TEX_MIN_X,TEX_MAX_Y)),//-1,0
					
					//TODO: add other planes?
			};
			
			int[] indices = new int[]{
					
					0,1,2,
					0,2,3,					
					
			};
			
			gunMesh = new Mesh(vertices,indices);
		}
		
		if(gunMaterial == null) {
			gunMaterial = new Material(gunAnimation.get(0));
		}
		
		this.gunTransform = new Transform();
		this.gunTransform.setTranslation(15.7f,0.4375f,3f);
		this.camera = new Camera(position,new Vector3f(0,0,1),new Vector3f(0,1,0));
		this.rand = new Random();
		this.health = MAX_HEALTH;
		this.movementVector = ZERO_VECTOR;
	}
	
	private void shootEnemy() {
		Sound.playClip("Pistol.wav");
		Vector2f lineStart = new Vector2f(camera.getPosition().getX(),camera.getPosition().getZ());
		Vector2f castDirection = new Vector2f(camera.getForward().getX(), camera.getForward().getZ()).normalized(); //if we wont normalize shoot distance will depend how forward (not ground ceilieng ) we are aiming
		Vector2f lineEnd = lineStart.add(castDirection.mul(SHOOT_DISTANCE));
		Game.getLevel().checkIntersections(lineStart, lineEnd,true);
		playShootAnimation = true;
		canShoot = false;
	}
	
	public void heal(int amount) {
		health += amount;
		if(health>MAX_HEALTH) {
			health = MAX_HEALTH;
		}
		System.out.println("HEALTH: "+health);
		Sound.playClip("Health.wav");

	}
	
	public void damage(int amount) {
		health -= amount;
		if(health>MAX_HEALTH) {
			health = MAX_HEALTH;
		}
		System.out.println("HEALTH: "+health);
		Sound.playClip("Player_Pain_1.wav");
		if(health<0) {
			//TODO: game over, can be done better as well I thinkg.
			Game.setIsRunning(false);
			System.out.println("GAME OVER!");
		}
	}
	
	public void input() {		
		
		float rotAmt = (float)(MOUSE_SENSITIVITY*Time.getDelta());
		
		if(Input.getKey(Input.KEY_ESCAPE)) {
			Input.setCursor(true);
			mouseLocked = false;
		}
		if(Input.getMouseDown(0)) {
			if(!mouseLocked) {
				Input.setMousePosition(centerPosition);
				Input.setCursor(false);
				mouseLocked = true;
			}else {
				if(canShoot) {
					shootEnemy();
				}
			
			}
			
			
		}
		
		if(Input.getKeyDown(Input.KEY_SPACE)) {
			Game.getLevel().openDoors(camera.getPosition(),true);
		}
		
		movementVector = ZERO_VECTOR;
		
		if(Input.getKey(Input.KEY_W)) {
			//camera.move(camera.getForward(),movAmt);
			movementVector = movementVector.add(camera.getForward());
		}
		if(Input.getKey(Input.KEY_S)) {
			//camera.move(camera.getForward(),-movAmt);
			movementVector = movementVector.sub(camera.getForward());
		}
		if(Input.getKey(Input.KEY_A)) {
			//camera.move(camera.getLeft(),movAmt);
			movementVector = movementVector.add(camera.getLeft());
		}
		if(Input.getKey(Input.KEY_D)) {
			//camera.move(camera.getRight(),movAmt);
			movementVector = movementVector.add(camera.getRight());
		}		
		
		if(Input.getKey(Input.KEY_UP)) {
			camera.rotateX(-rotAmt);
		}
		if(Input.getKey(Input.KEY_DOWN)) {
			camera.rotateX(rotAmt);
		}
		if(Input.getKey(Input.KEY_LEFT)) {
			camera.rotateY(-rotAmt);
		}
		if(Input.getKey(Input.KEY_RIGHT)) {
			camera.rotateY(rotAmt);
		}
		
		if(mouseLocked) {
			Vector2f deltaPos = Input.getMousePosition().sub(centerPosition);
			boolean rotY = deltaPos.getX() != 0;
			boolean rotX = deltaPos.getY() != 0;
			
			if(rotY) {
				camera.rotateY(deltaPos.getX()*MOUSE_SENSITIVITY);
			}
//			if(rotX) {
//				camera.rotateX(-deltaPos.getY()*MOUSE_SENSITIVITY);
//			}
			if(rotY||rotX) {
				Input.setMousePosition(centerPosition);
			}
			
		}
	}
	
	public int getHealth() {
		return this.health;
	}
	
	public void update() {
		
		float movAmt = (float)(MOVE_SPEED*Time.getDelta());
		movementVector.setY(0);
		if(movementVector.length()>0) {
			movementVector = movementVector.normalize();
		}		
		
		Vector3f oldPosition = camera.getPosition();
		Vector3f newPosition = oldPosition.add(movementVector.mul(movAmt));
		
		Vector3f collisionVector = Game.getLevel().checkCollisions(oldPosition, newPosition,PLAYER_SIZE,PLAYER_SIZE);
		movementVector = movementVector.mul(collisionVector);
	
		if(movementVector.length()>0) {
			camera.move(movementVector, movAmt);
		}		
		
		//Shoot animation
		double time = Time.getTime()/(double)Time.SECOND;
		
		if(playShootAnimation) {
			if(shootTime == 0) {
				shootTime = time;
			}
			
			//TODO: sometimes have to figure out anim timing (again, can it be automated?)
			//also scale values are a bit off
			if(time < shootTime + SHOOT_FRAME_T1) {
				gunMaterial.setTexture(gunAnimation.get(0));
			}else
			if(time < shootTime + SHOOT_FRAME_T2) {
				gunMaterial.setTexture(gunAnimation.get(1));
			}else
			if(time < shootTime + SHOOT_FRAME_T3) {
				gunMaterial.setTexture(gunAnimation.get(2));
			}else
			if(time < shootTime + SHOOT_FRAME_T4) {
				gunMaterial.setTexture(gunAnimation.get(1));	
			}else {
				gunMaterial.setTexture(gunAnimation.get(0));	
				shootTime = 0;
				canShoot = true;
				playShootAnimation = false;
			}		
		}//endIF
		
		//Gun movement //copied from Monster.faceCamera
		
				gunTransform.setTranslation(camera.getPosition().add(camera.getForward().normalize().mul(GUN_CAMERA_VECTOR_CONSTANT)));
				gunTransform.getTranslation().setY(gunTransform.getTranslation().getY()+GUN_OFFSET);
				Vector3f directionToCamera = Transform.getCamera().getPosition().sub(gunTransform.getTranslation());
				
				float angleToFaceTheCamera = (float)Math.toDegrees(Math.atan(directionToCamera.getZ()/directionToCamera.getX()));
				if(directionToCamera.getX()<0) {
					angleToFaceTheCamera+=180;
				}
				gunTransform.getRotation().setY(angleToFaceTheCamera+90);
		
	}
	
	
	public void render() {
		Shader shader = Game.getLevel().getShader(); //TODO: check if really have to grab shader object every render
		shader.updateUniforms(gunTransform.getTransformation(), gunTransform.getProjectedTransformation(), gunMaterial);
		gunMesh.draw();
	}

	public Camera getCamera() {
		return camera;
	}

	public int getDamage() {
		return rand.nextInt(DAMAGE_MAX-DAMAGE_MIN)+DAMAGE_MIN;
	}

}
