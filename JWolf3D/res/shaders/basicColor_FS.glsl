#version 330

in vec4 color;

out vec4 fragColor;

uniform vec3 inColor;

void main()
{
	fragColor = vec4(inColor,1);
	//fragColor = color;
}
