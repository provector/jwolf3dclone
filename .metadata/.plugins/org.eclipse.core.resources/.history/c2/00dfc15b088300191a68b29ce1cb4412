package provector.ie.jengine;

import java.util.ArrayList;

import provector.ie.jengine.components.Bitmap;
import provector.ie.jengine.components.Util;
import provector.ie.jengine.components.Window;
import provector.ie.jengine.rendering.Camera;
import provector.ie.jengine.rendering.Mesh;
import provector.ie.jengine.rendering.Transform;
import provector.ie.jengine.rendering.entities.Material;
import provector.ie.jengine.rendering.entities.Texture;
import provector.ie.jengine.rendering.entities.Vector2f;
import provector.ie.jengine.rendering.entities.Vector3f;
import provector.ie.jengine.rendering.entities.Vertex;
import provector.ie.jengine.rendering.shaders.BasicShader;
import provector.ie.jengine.rendering.shaders.Shader;

public class Game {
	
	Bitmap level;
	Shader shader;
	Material material;
	Mesh mesh;
	Transform transform;
	
	private static final float SPOT_WIDTH = 1f;
	private static final float SPOT_LENGTH = 1f;
	private static final float SPOT_HEIGHT = 1f;
	
	private static final int NUM_TEX_EXPONENT = 4; //we have 16 textures sqrt/16
	private static final int NUM_TEXTURES = (int) Math.pow(2,NUM_TEX_EXPONENT);
	
	public Game() {
		
		level = new Bitmap("TestLevel0.png").flipY();
		
		shader = BasicShader.getInstance();
		material = new Material(new Texture("TexturePack1.png"));	
		
		
		
		
		//Generate Level
		ArrayList<Vertex> vertices = new ArrayList<>();
		ArrayList<Integer> indices = new ArrayList<>();
				
		for(int i=0;i<level.getWidth();i++) {
			for(int j=0;j<level.getHeight();j++) 
			{
				if((level.getPixel(i, j)&0xFFFFFF)==0) {
					continue; //if black skip
				}
				
					
					
				//Ceiling
				
					//Texture
					int texX = ((level.getPixel(i, j)& 0x00FF00) >> 8)/NUM_TEXTURES;
					int texY = texX % NUM_TEX_EXPONENT;
					texX /= NUM_TEX_EXPONENT;
									
					float xHigher = 1f-(float)texX/(float)NUM_TEX_EXPONENT;
					float xLower = xHigher-1/(float)NUM_TEX_EXPONENT;
					float yLower = 1f-(float)texY/(float)NUM_TEX_EXPONENT;
					float yHigher = yLower-1/(float)NUM_TEX_EXPONENT;
						
					//texture faces
					indices.add(vertices.size()+0);
					indices.add(vertices.size()+1);
					indices.add(vertices.size()+2);
					
					indices.add(vertices.size()+0);
					indices.add(vertices.size()+2);
					indices.add(vertices.size()+3);				
					
					//vertices
					vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, SPOT_HEIGHT,   j *SPOT_LENGTH),new Vector2f(xLower,yLower)));
					vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, SPOT_HEIGHT,   j *SPOT_LENGTH),new Vector2f(xHigher,yLower)));
					vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, SPOT_HEIGHT,(j+1)*SPOT_LENGTH),new Vector2f(xHigher,yHigher)));
					vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, SPOT_HEIGHT,(j+1)*SPOT_LENGTH),new Vector2f(xLower,yHigher)));
					
				//Floor
					
					//texture faces
					indices.add(vertices.size()+2);
					indices.add(vertices.size()+1);
					indices.add(vertices.size()+0);
					
					indices.add(vertices.size()+3);
					indices.add(vertices.size()+2);
					indices.add(vertices.size()+0);				
					
					//vertices
					vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, 0,   j *SPOT_LENGTH),new Vector2f(xLower,yLower)));
					vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, 0,   j *SPOT_LENGTH),new Vector2f(xHigher,yLower)));
					vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, 0,(j+1)*SPOT_LENGTH),new Vector2f(xHigher,yHigher)));
					vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, 0,(j+1)*SPOT_LENGTH),new Vector2f(xLower,yHigher)));
				
				//Walls
					
					//Texture
					texX = ((level.getPixel(i, j)& 0xFF0000) >> 16)/NUM_TEXTURES;
					texY = texX % NUM_TEX_EXPONENT;
					texX /= NUM_TEX_EXPONENT;
									
					xHigher = 1f-(float)texX/(float)NUM_TEX_EXPONENT;
					xLower = xHigher-1/(float)NUM_TEX_EXPONENT;
					yLower = 1f-(float)texY/(float)NUM_TEX_EXPONENT;
					yHigher = yLower-1/(float)NUM_TEX_EXPONENT;
					
					
					//Z axis
					if((level.getPixel(i, j-1) &0xFFFFFF)==0) { //check if adjacent pixel is black
						//texture faces
						indices.add(vertices.size()+0);
						indices.add(vertices.size()+1);
						indices.add(vertices.size()+2);
						
						indices.add(vertices.size()+0);
						indices.add(vertices.size()+2);
						indices.add(vertices.size()+3);				
						
						//vertices
						vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, 0,   j *SPOT_LENGTH),new Vector2f(xLower,yLower)));
						vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, 0,   j *SPOT_LENGTH),new Vector2f(xHigher,yLower)));
						vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, SPOT_HEIGHT,	j*SPOT_LENGTH),new Vector2f(xHigher,yHigher)));
						vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, SPOT_HEIGHT,	j*SPOT_LENGTH),new Vector2f(xLower,yHigher)));
					}
					
					//other direction
					if((level.getPixel(i, j+1) &0xFFFFFF)==0) { //check if adjacent pixel is black
						//texture faces
						indices.add(vertices.size()+2);
						indices.add(vertices.size()+1);
						indices.add(vertices.size()+0);
						
						indices.add(vertices.size()+3);
						indices.add(vertices.size()+2);
						indices.add(vertices.size()+0);				
						
						//vertices
						vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, 0,   (j+1) *SPOT_LENGTH),new Vector2f(xLower,yLower)));
						vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, 0,   (j+1) *SPOT_LENGTH),new Vector2f(xHigher,yLower)));
						vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, SPOT_HEIGHT,	(j+1)*SPOT_LENGTH),new Vector2f(xHigher,yHigher)));
						vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, SPOT_HEIGHT,	(j+1)*SPOT_LENGTH),new Vector2f(xLower,yHigher)));
					}
					
					//Xaxis
					if((level.getPixel(i-1, j) &0xFFFFFF)==0) { //check if adjacent pixel is black
						//texture faces
						indices.add(vertices.size()+2);
						indices.add(vertices.size()+1);
						indices.add(vertices.size()+0);
						
						indices.add(vertices.size()+3);
						indices.add(vertices.size()+2);
						indices.add(vertices.size()+0);				
						
						//vertices
						vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, 			 0,    j*SPOT_LENGTH),new Vector2f(xLower,yLower)));
						vertices.add(new Vertex(new Vector3f(	i *SPOT_WIDTH, 			 0,(j+1)*SPOT_LENGTH),new Vector2f(xHigher,yLower)));
						vertices.add(new Vertex(new Vector3f(	i *SPOT_WIDTH, SPOT_HEIGHT,(j+1)*SPOT_LENGTH),new Vector2f(xHigher,yHigher)));
						vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, SPOT_HEIGHT,    j*SPOT_LENGTH),new Vector2f(xLower,yHigher)));
					}
					
					//Xaxis - other direction
					if((level.getPixel(i+1, j) &0xFFFFFF)==0) { //check if adjacent pixel is black
						//texture faces
						indices.add(vertices.size()+0);
						indices.add(vertices.size()+1);
						indices.add(vertices.size()+2);
						
						indices.add(vertices.size()+0);
						indices.add(vertices.size()+2);
						indices.add(vertices.size()+3);				
						
						//vertices
						vertices.add(new Vertex(new Vector3f((i+1) *SPOT_WIDTH, 			 0,    j*SPOT_LENGTH),new Vector2f(xLower,yLower)));
						vertices.add(new Vertex(new Vector3f((i+1) *SPOT_WIDTH, 			 0,(j+1)*SPOT_LENGTH),new Vector2f(xHigher,yLower)));
						vertices.add(new Vertex(new Vector3f((i+1) *SPOT_WIDTH, SPOT_HEIGHT,(j+1)*SPOT_LENGTH),new Vector2f(xHigher,yHigher)));
						vertices.add(new Vertex(new Vector3f((i+1) *SPOT_WIDTH, SPOT_HEIGHT,    j*SPOT_LENGTH),new Vector2f(xLower,yHigher)));
					}
					
			}
		}
		
		Vertex[] vertArray = new Vertex[vertices.size()];
		Integer[] intArray = new Integer[indices.size()];
		
		vertices.toArray(vertArray);
		indices.toArray(intArray);
		
		mesh = new Mesh(vertArray,Util.toIntArray(intArray),false);
		//mesh = new Mesh("cube.obj",true);
					
		transform = new Transform();
		Transform.setCamera(new Camera());
		Transform.setProjection(70,Window.getWidth(), Window.getHeight(), 0.01f, 1000f);
		
//		Vertex[] vertices = new Vertex[] {
//			new Vertex(new Vector3f(0,0,0),new Vector2f(0,0)),
//			new Vertex(new Vector3f(0,1,0),new Vector2f(0,1)),
//			new Vertex(new Vector3f(1,1,0),new Vector2f(1,1)),
//			new Vertex(new Vector3f(1,0,0),new Vector2f(1,0))
//		};
//		
//		int[] indices = new int[] {
//				0,1,2,
//				0,2,3
//		};
//		mesh = new Mesh(vertices,indices,true);
		
	}
	
	public void input() {
		Transform.getCamera().input();
	}
	
	public void update() {
		
	}
	
	public void render() {
	
		shader.bind(); //TODO: this should be done only once at init I think
		shader.updateUniforms(transform.getTransformation(),transform.getProjectedTransformation(), material);
		mesh.draw();
	}
	
	/*
	 * TODOS:
	 * 
	 * [] - For now filtering is disabled, figure out a better way to load separate textures (GL_NEAREST,Texture.Class);
	 */
	
}