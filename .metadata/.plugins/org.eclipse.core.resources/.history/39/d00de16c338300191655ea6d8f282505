package provector.ie.jengine;

import java.util.ArrayList;

import provector.ie.jengine.components.Bitmap;
import provector.ie.jengine.components.Util;
import provector.ie.jengine.rendering.Mesh;
import provector.ie.jengine.rendering.Transform;
import provector.ie.jengine.rendering.entities.Material;
import provector.ie.jengine.rendering.entities.Texture;
import provector.ie.jengine.rendering.entities.Vector2f;
import provector.ie.jengine.rendering.entities.Vector3f;
import provector.ie.jengine.rendering.entities.Vertex;
import provector.ie.jengine.rendering.shaders.BasicShader;
import provector.ie.jengine.rendering.shaders.Shader;

public class Level {
	
	private static final float SPOT_WIDTH = 1f;
	private static final float SPOT_LENGTH = 1f;
	private static final float SPOT_HEIGHT = 1f;
	
	private static final int NUM_TEX_EXPONENT = 4; //we have 16 textures sqrt/16
	private static final int NUM_TEXTURES = (int) Math.pow(2,NUM_TEX_EXPONENT);
	
	private Mesh mesh;
	private Bitmap level;
	private Shader shader;
	private Material material;
	private Transform transform;

	public Level(String levelFile,String textureFile) {
	
		level = new Bitmap(levelFile).flipY();	
		
		material = new Material(new Texture(textureFile));			
		shader = BasicShader.getInstance();						
		transform = new Transform();
		
		generateLevel();
	}
	
	public void input() {
		
	}
	
	public void update() {
		
	}
	
	public void render() {
		shader.bind(); //TODO: this should be done only once at init I think
		shader.updateUniforms(transform.getTransformation(),transform.getProjectedTransformation(), material);
		mesh.draw();
	}
	
	private void addFace(ArrayList<Integer> indices,int startLocation,boolean direction) {
		if(direction) {
			indices.add(startLocation+2);
			indices.add(startLocation+1);
			indices.add(startLocation+0);
			
			indices.add(startLocation+3);
			indices.add(startLocation+2);
			indices.add(startLocation+0);				
		}else {
			indices.add(startLocation+0);
			indices.add(startLocation+1);
			indices.add(startLocation+2);
			
			indices.add(startLocation+0);
			indices.add(startLocation+2);
			indices.add(startLocation+3);			
		}
	}
	
	private float[] calcTexCoords(int baseColorShift) {
		int texX = baseColorShift/NUM_TEXTURES;
		int texY = texX % NUM_TEX_EXPONENT;
		texX /= NUM_TEX_EXPONENT;
						
		float[] result = new float[4];
		
		result[0] = 1f-(float)texX/(float)NUM_TEX_EXPONENT;
		result[1] = result[0]-1/(float)NUM_TEX_EXPONENT;
		result[3] = 1f-(float)texY/(float)NUM_TEX_EXPONENT;
		result[2] = result[3]-1/(float)NUM_TEX_EXPONENT;
		
		return result;
	}
	
	private void generateLevel() {		//Generate Level
		ArrayList<Vertex> vertices = new ArrayList<>();
		ArrayList<Integer> indices = new ArrayList<>();
				
		for(int i=0;i<level.getWidth();i++) {
			for(int j=0;j<level.getHeight();j++) 
			{
				if((level.getPixel(i, j)&0xFFFFFF)==0) {
					continue; //if black skip
				}
				
					
				/*
				 * So the color component we are shifting by is a coresponding component in gleditor (r,g,b);
				 * In example, ceiling ix 0x00FF00 which is green, green color slider value will determin floor/ceiling texture.
				 * So if we want texture 11 from matrix (counted bottom up then left from bottom right corner) we do 11*16 = 176
				 * and set green component to 176. Calculate the same for red component (walls) and fill the level file with appropriate color
				 */
					
				//Ceiling
				
					//Texture Coords
					float[] texCoords = calcTexCoords((level.getPixel(i, j) & 0x00FF00)>>8);
					
					float xHigher = texCoords[0];
					float xLower = texCoords[1];
					float yHigher = texCoords[2];
					float yLower = texCoords[3];
						
					//texture faces
					addFace(indices,vertices.size(),false);
					
					//vertices
					vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, SPOT_HEIGHT,   j *SPOT_LENGTH),new Vector2f(xLower,yLower)));
					vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, SPOT_HEIGHT,   j *SPOT_LENGTH),new Vector2f(xHigher,yLower)));
					vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, SPOT_HEIGHT,(j+1)*SPOT_LENGTH),new Vector2f(xHigher,yHigher)));
					vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, SPOT_HEIGHT,(j+1)*SPOT_LENGTH),new Vector2f(xLower,yHigher)));
					
				//Floor
					
					//texture faces
					addFace(indices,vertices.size(),true);
					
					//vertices
					vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, 0,   j *SPOT_LENGTH),new Vector2f(xLower,yLower)));
					vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, 0,   j *SPOT_LENGTH),new Vector2f(xHigher,yLower)));
					vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, 0,(j+1)*SPOT_LENGTH),new Vector2f(xHigher,yHigher)));
					vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, 0,(j+1)*SPOT_LENGTH),new Vector2f(xLower,yHigher)));
				
				//Walls
					
					//Texture 
					texCoords = calcTexCoords((level.getPixel(i, j) & 0xFF0000)>>16);
					
					xHigher = texCoords[0];
					xLower = texCoords[1];
					yHigher = texCoords[2];
					yLower = texCoords[3];
					
					
					//Z axis
					if((level.getPixel(i, j-1) &0xFFFFFF)==0) { //check if adjacent pixel is black
						//texture faces
						addFace(indices,vertices.size(),false);				
						
						//vertices
						vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, 0,   j *SPOT_LENGTH),new Vector2f(xLower,yLower)));
						vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, 0,   j *SPOT_LENGTH),new Vector2f(xHigher,yLower)));
						vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, SPOT_HEIGHT,	j*SPOT_LENGTH),new Vector2f(xHigher,yHigher)));
						vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, SPOT_HEIGHT,	j*SPOT_LENGTH),new Vector2f(xLower,yHigher)));
					}
					
					//other direction
					if((level.getPixel(i, j+1) &0xFFFFFF)==0) { //check if adjacent pixel is black
						//texture faces
						addFace(indices,vertices.size(),true);		
						
						//vertices
						vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, 0,   (j+1) *SPOT_LENGTH),new Vector2f(xLower,yLower)));
						vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, 0,   (j+1) *SPOT_LENGTH),new Vector2f(xHigher,yLower)));
						vertices.add(new Vertex(new Vector3f((i+1)*SPOT_WIDTH, SPOT_HEIGHT,	(j+1)*SPOT_LENGTH),new Vector2f(xHigher,yHigher)));
						vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, SPOT_HEIGHT,	(j+1)*SPOT_LENGTH),new Vector2f(xLower,yHigher)));
					}
					
					//Xaxis
					if((level.getPixel(i-1, j) &0xFFFFFF)==0) { //check if adjacent pixel is black
						//texture faces
						addFace(indices,vertices.size(),true);				
						
						//vertices
						vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, 			 0,    j*SPOT_LENGTH),new Vector2f(xLower,yLower)));
						vertices.add(new Vertex(new Vector3f(	i *SPOT_WIDTH, 			 0,(j+1)*SPOT_LENGTH),new Vector2f(xHigher,yLower)));
						vertices.add(new Vertex(new Vector3f(	i *SPOT_WIDTH, SPOT_HEIGHT,(j+1)*SPOT_LENGTH),new Vector2f(xHigher,yHigher)));
						vertices.add(new Vertex(new Vector3f(   i *SPOT_WIDTH, SPOT_HEIGHT,    j*SPOT_LENGTH),new Vector2f(xLower,yHigher)));
					}
					
					//Xaxis - other direction
					if((level.getPixel(i+1, j) &0xFFFFFF)==0) { //check if adjacent pixel is black
						//texture faces
						addFace(indices,vertices.size(),false);
						
						//vertices
						vertices.add(new Vertex(new Vector3f((i+1) *SPOT_WIDTH, 			 0,    j*SPOT_LENGTH),new Vector2f(xLower,yLower)));
						vertices.add(new Vertex(new Vector3f((i+1) *SPOT_WIDTH, 			 0,(j+1)*SPOT_LENGTH),new Vector2f(xHigher,yLower)));
						vertices.add(new Vertex(new Vector3f((i+1) *SPOT_WIDTH, SPOT_HEIGHT,(j+1)*SPOT_LENGTH),new Vector2f(xHigher,yHigher)));
						vertices.add(new Vertex(new Vector3f((i+1) *SPOT_WIDTH, SPOT_HEIGHT,    j*SPOT_LENGTH),new Vector2f(xLower,yHigher)));
					}
					
			}
		}
		
		Vertex[] vertArray = new Vertex[vertices.size()];
		Integer[] intArray = new Integer[indices.size()];
		
		vertices.toArray(vertArray);
		indices.toArray(intArray);
		
		mesh = new Mesh(vertArray,Util.toIntArray(intArray),false);;
	}
}
